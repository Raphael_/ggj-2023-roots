using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace roots
{
    public class BridgeLightController : MonoBehaviour
    {
        [SerializeField]
        private Light light;

        private Material defaultMaterial;
        [SerializeField] private Material pressedMaterial;
        private MeshRenderer _renderer;

        [SerializeField] private ButtonController button;
        // Start is called before the first frame update
        void Start()
        {
            _renderer = GetComponent<MeshRenderer>();
            defaultMaterial = _renderer.material;
            button.OnPressed.AddListener(() =>
                {
                    _renderer.material = pressedMaterial;
                    light.enabled = true;
                }
            );
            button.OnReleased.AddListener(() =>
            {
                _renderer.material = defaultMaterial;
                light.enabled = false;
            });
        }
    }
}
