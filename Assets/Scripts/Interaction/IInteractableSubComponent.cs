namespace roots
{
    public interface IInteractableSubComponent
    {
        public bool OnInteract();
    }
}