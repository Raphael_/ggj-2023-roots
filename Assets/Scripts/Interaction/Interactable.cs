using System;
using System.Collections.Generic;
using System.Linq;
using roots.ItemRelated;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace roots
{
    public class Interactable : MonoBehaviour
    {
        [field: SerializeField] public UnityEvent OnInteract { get; private set; }

        private AudioClip InteractionClip;

        private List<IInteractableSubComponent> interactableSubComponents = new List<IInteractableSubComponent>();

        private void Start()
        {
            interactableSubComponents = GetComponents<IInteractableSubComponent>().ToList();
        }

        public bool Interact()
        {
            print($"Interacted with {gameObject.name}");
            OnInteract.Invoke();

            bool success = true;
            foreach (var subComponent in interactableSubComponents)
            {
                if (subComponent.OnInteract() == false)
                {
                    success = false;
                }
            }

            return success;
        }

        [Button]
        private void AddItemRequirement()
        {
            gameObject.AddComponent<ItemRequired>();
        }

        [Button]
        private void AddItemPickup()
        {
            gameObject.AddComponent<ItemPickup>();
        }

        [Button]
        private void AddColliderTrigger()
        {
            var sphereCollider = gameObject.AddComponent<SphereCollider>();
            sphereCollider.isTrigger = true;
        }
    }
}