using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


namespace roots
{
    public class Menu : MonoBehaviour
    {

        [SerializeField] GameObject pauseMenu;
        [SerializeField] Slider volumeSlider;

        private bool isPaused;

        void Start()
        {
            ResumeGame();
            if(!PlayerPrefs.HasKey("Volume")){
                PlayerPrefs.SetFloat("Volume",1);
            }
        }

        void Update(){
            if(Input.GetKeyDown(KeyCode.Escape)){
                if(isPaused)
                {
                    ResumeGame();
                }
                else{
                    PauseGame();
                }
            }

        }

        
        public void ChangeVolume(){
            AudioListener.volume = volumeSlider.value;
            Save();
        }

        private void Load(){
            volumeSlider.value = PlayerPrefs.GetFloat("Volume");
        }
        
        private void Save(){
            PlayerPrefs.SetFloat("Volume", volumeSlider.value);
        }

        
        public void PauseGame(){
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            isPaused = true;
        }

        public void ResumeGame(){
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
            isPaused = false;
        }

        public void PlayGame()
        {
            SceneManager.LoadScene(1);
        }
        public void QuitGame()
        {
            Application.Quit();
        }
        public void LoadMainMenu(){
            SceneManager.LoadScene(0);
        }

    }
}
