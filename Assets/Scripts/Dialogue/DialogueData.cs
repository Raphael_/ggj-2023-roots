using System.Collections.Generic;
using UnityEngine;

namespace roots
{
    [CreateAssetMenu]
    public class DialogueData : ScriptableObject
    {
        [field: SerializeField] public Sprite sprite{ get; private set; }
        [field: SerializeField] public new string name { get; private set; }
        [field: SerializeField] public List<string> lines { get; private set; } = new();
    }
}