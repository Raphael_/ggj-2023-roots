using System;
using UnityEngine;
using UnityEngine.Events;

namespace roots.Dialogue
{
    [RequireComponent(typeof(Interactable))]
    public class DialogueTrigger : MonoBehaviour
    {
        [SerializeField] public DialogueData data;

        private Interactable interactable;
        
        private float maxDistance = 4f;
        private bool dialogueRunning = false;

        [field: SerializeField]
        public UnityEvent onConversationOver { get; private set; } = new();
        [field: SerializeField]
        public UnityEvent onConversationInterrupted { get; private set; } = new();

        private void Awake()
        {
            interactable = GetComponent<Interactable>();
        }

        private void OnDialogueDone(bool interrupted)
        {
            if (interrupted)
            {
                onConversationOver.Invoke();
            }
            else
            {
                onConversationInterrupted.Invoke();
            }
            
            dialogueRunning = false;
            interactable.enabled = true;
        }

        private void FixedUpdate()
        {
            if (dialogueRunning)
            {
                var distance = Vector3.Distance(transform.position, Player.Instance.transform.position);

                if (distance > maxDistance)
                {
                    DialogueSystem.Instance.InterruptAndStopPlayback();
                }
            }
        }

        private void Start()
        {
            interactable.OnInteract.AddListener(StartDialog);
        }

        public void StartDialog()
        {
            if (dialogueRunning)
            {
                return;
            }
            if (DialogueSystem.Instance.StartDialogue(data.sprite, data.name, data.lines, OnDialogueDone))
            {
                dialogueRunning = true;
                interactable.enabled = false;
            }
        }
    }
}