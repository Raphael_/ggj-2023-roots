using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace roots
{
    public class DialogueSystem : MonoBehaviour
    {
        [SerializeField] private Image talkerImage;
        [SerializeField] private Transform talkerRoot;
        [SerializeField] private TMP_Text talkerName;
        [SerializeField] private TMP_Text talkTextBox;

        private Queue<string> toBeDisplayedDialogue;

        [SerializeField] private Animator animator;
        [SerializeField] private Sprite debug;

        private bool running = false;
        private static readonly int Visible = Animator.StringToHash("Visible");

        [SerializeField] private float letterAppearSpeed = 0.05f;

        public static DialogueSystem Instance;

        private Action<bool> completedAction;

        private void Awake()
        {
            Instance = this;
            toBeDisplayedDialogue = new Queue<string>();
        }


        [Button]
        private void DebugDialoguee()
        {
            StartDialogue(debug, "DEBUG CHAR",
                new List<string>()
                {
                    "Hello there", "Nice weather have we right?", "Sure a lot of crows though...",
                    "Would be nice if someone could do something about them, they're a bit of a bother",
                    "You see its common knowledge my crops are delicious, but unfortunetely it seems the crows are also privy to such information"
                }, b=>{});
        }

        public bool StartDialogue(Sprite talkerSprite, string name, List<string> lines, Action<bool> completed)
        {
            if (running)
            {
                return false;
            }

            running = true;
            talkerImage.sprite = talkerSprite;
            talkerName.text = name;
            toBeDisplayedDialogue.Clear();
            foreach (var line in lines)
            {
                toBeDisplayedDialogue.Enqueue(line);
            }
            
            talkTextBox.text = "";

            animator.SetBool(Visible, true);
            
            DisplayNextLine();

            completedAction = completed;

            return true;
        }



        public void InterruptAndStopPlayback()
        {
            if(!running)
                return;

            nextLineToDo = "";
            
            completedAction(true);
            running = false;
            animator.SetBool(Visible, false);
        }

        public bool DisplayNextLine()
        {
            if (displayingNextLine)
            {
                return false;
            }

            if (toBeDisplayedDialogue.TryDequeue(out var nextLine))
            {
                nextLineToDo = nextLine;
                displayingNextLine = true;
                talkTextBox.text = nextLineToDo;
                talkTextBox.ForceMeshUpdate();
                totalVisibleCharacters = talkTextBox.textInfo.characterCount;
                talkTextBox.maxVisibleCharacters = 0;
                counter = 0;
                return true;
            }
            else
            {
                running = false;
                animator.SetBool(Visible, false);
                completedAction(false);
                return false;
            }
        }

        private bool displayingNextLine = false;

        private float floatNextLetterTime;

        private string nextLineToDo;
        private int totalVisibleCharacters;
        private int counter = 0;

        private float waitForNextLine;
        
        
        private void Update()
        {
            if (!running)
            {
                return;
            }
            
            if (displayingNextLine)
            {
                if (Time.time > floatNextLetterTime)
                {
                    if (counter < totalVisibleCharacters+1)
                    {
                        counter++;
                        talkTextBox.maxVisibleCharacters = counter;
                        talkerRoot.DOShakeRotation(letterAppearSpeed, 5f, 3);
                        floatNextLetterTime = Time.time + letterAppearSpeed;
                    }
                    else
                    {
                        displayingNextLine = false;
                        var timeNext = nextLineToDo.Length*0.023f + 0.6f;
                        waitForNextLine = Time.time + timeNext;
                    }
                }
            }
            else
            {
                if (Time.time > waitForNextLine)
                {
                    DisplayNextLine();
                }
            }
        }
    }
}