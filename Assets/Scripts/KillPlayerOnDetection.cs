using UnityEngine;

namespace roots
{
    public class KillPlayerOnDetection : MonoBehaviour
    {
        public void KillPlayer()
        {
            Player.Instance.playerDeathManager.Die();
        }
    }
}