using System.Linq;
using Micosmo.SensorToolkit;
using UnityEngine;
using UnityEngine.Events;

namespace roots
{
    [RequireComponent(typeof(RangeSensor))]
    public class ButtonController : MonoBehaviour
    {
        private Material defaultMaterial;
        [SerializeField] private Material pressedMaterial;
        [SerializeField] private MeshRenderer buttonRenderer;

        private RangeSensor rangeSensor;

        public UnityEvent OnPressed { get; private set; } = new();
        public UnityEvent OnReleased { get; private set; } = new();

        private void Awake()
        {
            rangeSensor = GetComponent<RangeSensor>();
        }

        void Start()
        {
            defaultMaterial = buttonRenderer.material;
        }

        // Update is called once per frame
        void Update()
        {
        }

        public void PressButton()
        {
            OnPressed.Invoke();
            buttonRenderer.material = pressedMaterial;
        }

        public void UnpressButton()
        {
            if (rangeSensor.Detections.Count < 1)
            {
                OnReleased.Invoke();
                buttonRenderer.material = defaultMaterial;
            }
        }
    }
}