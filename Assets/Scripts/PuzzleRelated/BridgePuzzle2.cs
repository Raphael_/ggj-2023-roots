using System;
using DG.Tweening;
using Micosmo.SensorToolkit;
using roots.Dialogue;
using Sirenix.OdinInspector;
using UnityEngine;

namespace roots
{

    enum BridgeState
    {
        High,
        Low,
    }
    
    public class BridgePuzzle2 : MonoBehaviour
    {
        private int RiverBlockingsCount = 0;

        [SerializeField] private Animator _animator;

        private BridgeState BridgeState = BridgeState.High;
        [SerializeField] private GameObject Bridge;

        [SerializeField]
        private ButtonController button1;

        private bool button1Pressed;
        [SerializeField]
        private ButtonController button2;
        private bool button2Pressed;

        private Quaternion desiredRotation;

        private bool allButtonsPressed => button1Pressed && button2Pressed;

        [SerializeField]
        private RangeSensor RiverDeathTrigger;

        private bool loweredBridgeOnce = false;

        [SerializeField] private DialogueTrigger dialogueTrigger;
        [SerializeField] private DialogueData dialogueData;

        /*[Button]
        private void SetLowRotation()
        {
            lowRotation = transform.rotation;
        }
        
        [Button]
        private void SetHighRotation()
        {
            highRotation = transform.rotation;
        }*/

        // Start is called before the first frame update
        void Start()
        {
            button1.OnPressed.AddListener(() => button1Pressed = true);
            button1.OnReleased.AddListener(() => button1Pressed = false);
            
            button2.OnPressed.AddListener(() => button2Pressed = true);
            button2.OnReleased.AddListener(() => button2Pressed = false);
        }

        // Update is called once per frame
        void Update()
        {
            BridgeState = allButtonsPressed ? BridgeState.Low : BridgeState.High;
            if (allButtonsPressed && loweredBridgeOnce == false)
            {
                loweredBridgeOnce = true;
                dialogueTrigger.data = dialogueData;
            }
            
            RiverDeathTrigger.enabled = !allButtonsPressed;
            RotateBridgeAccordingToState(BridgeState);
        }

        private void RotateBridgeAccordingToState(BridgeState bridgeState)
        {
            switch (bridgeState)
            {
                case BridgeState.High:
                    _animator.SetBool("Lowered", false);
                    break;
                case BridgeState.Low:
                    _animator.SetBool("Lowered", true);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(bridgeState), bridgeState, null);
            }
        }

        
    }
}