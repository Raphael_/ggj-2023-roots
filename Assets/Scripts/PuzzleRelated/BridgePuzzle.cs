using roots.PuzzleRelated;
using UnityEngine;

namespace roots
{

    public class BridgePuzzle : MonoBehaviour
    {
        [SerializeField] private int RequiredRiverBlockings = 3;
        private int RiverBlockingsCount = 0;

        private BridgeState BridgeState = BridgeState.High;
        [SerializeField] private GameObject HighBridge;
        [SerializeField] private GameObject LowBridge;

        [SerializeField] private float MinWaterY = -0.37f;
        [SerializeField] private float MaxWaterY = 0.04f;

        [SerializeField] private GameObject DefaultWaterPathObject;

        [SerializeField] private GameObject AlternateWaterPathObject;

        [SerializeField] private MillWheelRotator MillWheelRotator;

        private bool RequiredBlockingsCompleted => RequiredRiverBlockings / RequiredRiverBlockings == 1;

        // Start is called before the first frame update
        void Start()
        {
            UpdateWaterLevel();
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void LowerBridge()
        {
            BridgeState = BridgeState.Low;
            LowBridge.SetActive(true);
            HighBridge.SetActive(false);
        }

        public void BlockRiver()
        {
            if (RequiredBlockingsCompleted) return;
            RiverBlockingsCount++;
            UpdateWaterLevel();
            if (RequiredBlockingsCompleted)
            {
                MillWheelRotator.enabled = true;
                LowerBridge();
            }
        }

        private void UpdateWaterLevel()
        {
            var defaultWaterPathObjectPosition = DefaultWaterPathObject.transform.position;
            var blockingProgress = (float)RiverBlockingsCount / RequiredRiverBlockings;
            DefaultWaterPathObject.transform.position = new Vector3(
                defaultWaterPathObjectPosition.x,
                Mathf.Lerp(MaxWaterY, MinWaterY, blockingProgress),
                defaultWaterPathObjectPosition.z
            );

            var alternateWaterPathObjectPosition = AlternateWaterPathObject.transform.position;
            AlternateWaterPathObject.transform.position = new Vector3(
                alternateWaterPathObjectPosition.x,
                Mathf.Lerp(MinWaterY, MaxWaterY, blockingProgress),
                alternateWaterPathObjectPosition.z
            );
        }
    }
}