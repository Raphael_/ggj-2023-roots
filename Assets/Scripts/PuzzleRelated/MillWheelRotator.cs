using UnityEngine;

namespace roots.PuzzleRelated
{
    public class MillWheelRotator : MonoBehaviour
    {
        [SerializeField] private float RotationMultiplier = 9;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(Vector3.right, Time.deltaTime * RotationMultiplier);
        }
    }
}
