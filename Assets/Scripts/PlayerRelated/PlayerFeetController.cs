using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace roots
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerFeetController : MonoBehaviour
    {
        [SerializeField] private Transform playerFoot;

        private new Rigidbody rigidbody;

        [SerializeField] private Transform raycastOrigin;
        [SerializeField] private Transform jumpFootPostion;

        private Vector3 worldFootPosition;
    
        [SerializeField] private float footMoveThreshold = 0.7f;
        [SerializeField] private float footRaiseFactor = 0.5f;
        [SerializeField] private float velocityDistanceMultiplier = 1f;

        [SerializeField] private LayerMask layerMask;
        
        public readonly UnityEvent OnFootStep = new UnityEvent();

        private void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            worldFootPosition = playerFoot.position;
        }

        private void LateUpdate()
        {
            playerFoot.position = worldFootPosition;

            var leftDistance = Vector3.Distance(playerFoot.position, raycastOrigin.position);

            if (Player.Instance.playerMovementController.Grounded && leftDistance > footMoveThreshold)
            {
                //UpdateFootPosition();
            }
            else if (!Player.Instance.playerMovementController.Grounded)
            {
                JumpFootPosition();
            }
        }

        private void JumpFootPosition()
        {
            worldFootPosition = Vector3.Lerp(worldFootPosition, jumpFootPostion.position, 0.4f * Time.deltaTime * 60f);
        }

        public void UpdateFootPosition(bool withVelocity = true)
        {
            var velocity = withVelocity ? rigidbody.velocity : Vector3.zero;
            velocity = new Vector3(velocity.x, 0f, velocity.z);
            
            if(Physics.Raycast(raycastOrigin.position + velocity * velocityDistanceMultiplier, Vector3.down, out var info, 1.3f, layerMask))
            {
                if (doingAnimation == false)
                {
                    OnFootStep.Invoke();
                    FootArc(info.point);
                }
            }
        }

        private bool doingAnimation;

        private void FootArc(Vector3 endPos)
        {
            doingAnimation = true;
            Vector3 startPos = worldFootPosition;
            Vector3 midPoint = Vector3.Lerp(startPos, endPos, 0.5f) + Vector3.up*footRaiseFactor;

            DOTween.To(() => worldFootPosition, value => worldFootPosition = value, midPoint, 0.1f).OnComplete(() =>
            {
                DOTween.To(() => worldFootPosition, value => worldFootPosition = value, endPos, 0.1f).OnComplete(() =>
                {
                    doingAnimation = false;
                });
            });


        }
    }
}