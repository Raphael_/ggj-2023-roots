using System;
using DG.Tweening;
using roots.PlayerRelated;
using UnityEngine;

namespace roots
{
    public class Player : MonoBehaviour
    {
        public static Player Instance;

        public ItemInventory itemInventory { get; private set; }
        public PlayerDeathManager playerDeathManager { get; private set; }
        public PlayerMovementController playerMovementController { get; private set; }
        public PlayerSoundController playerSoundController { get; private set; }

        public EnvironmentSoundController environmentSoundController { get; private set; }

        private void Awake()
        {
            Instance = this;

            itemInventory = GetComponent<ItemInventory>();
            playerDeathManager = GetComponent<PlayerDeathManager>();
            playerMovementController = GetComponent<PlayerMovementController>();
            playerSoundController = GetComponent<PlayerSoundController>();
        }
    }
}