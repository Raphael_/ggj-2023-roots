using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace roots
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerMovementController : MonoBehaviour
    {
        private new Rigidbody rigidbody;
        [SerializeField] private Transform groundCheck;
        [SerializeField] private LayerMask groundLayerMask;

        [SerializeField] private float speed = 3f;
        [SerializeField] private float jumpForce = 5f;

        [SerializeField] private float forwardAlignmentSmoothing = 0.3f;
        [SerializeField] private float movementChangeSmoothing = 0.5f;

        [SerializeField] private Transform bodyTransform;
        [SerializeField] private float bobSpeed = 1f;
        [SerializeField] private float bobAmplitude = 1f;
        [SerializeField] private float movementSpeedBobInfluenceAmplitutde = 3f;
        [SerializeField] private float movementSpeedBobInfluenceFrequency = 2f;

        [SerializeField] private AudioSource jumpAudio;
        private Vector3 startPos;

        private bool grounded_BackingField;

        public bool Grounded
        {
            get => grounded_BackingField;
            private set
            {
                grounded_BackingField = value;
                if (value)
                {
                    var dustParticlesEmission = dustParticles.emission;
                    dustParticlesEmission.rateOverDistance = 6f;
                }
                else
                {
                    var dustParticlesEmission = dustParticles.emission;
                    dustParticlesEmission.rateOverDistance = 0f;
                }
            }
        }

        private Collider[] groundCheckArray = new Collider[1];

        [SerializeField] private Vector2 desiredMovement = Vector2.zero;
        private Vector2 movementDirection = Vector2.zero;

        [SerializeField] private ParticleSystem dustParticles;

        [SerializeField] private PlayerFeetController rightController;
        [SerializeField] private PlayerFeetController leftController;


        private void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
            startPos = bodyTransform.transform.localScale;
            lastFramePosition = transform.position;

            InitStepIntervals();
        }

        private void InitStepIntervals()
        {
            lastStepIntervalRight = 0f;
            lastStepIntervalLeft = stepSize * .5f;
        }

        private void Start()
        {
            Player.Instance.playerDeathManager.OnPlayerDied.AddListener(() =>
            {
                Invoke(nameof(OnPlayerDiedFeetUpdate), 0.17f);
            });
        }

        private void OnPlayerDiedFeetUpdate()
        {
            UpdateFeetPosition(false);
        }

        private void UpdateFeetPosition(bool withVelocity = true )
        {
            rightController.UpdateFootPosition(withVelocity);
            leftController.UpdateFootPosition(withVelocity);
        }

        private void OnJump()
        {
            if (Grounded)
            {
                rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
                jumpAudio.Play();
            }
        }


        private float sinValue;

        private void OnMove(InputValue value)
        {
            //print("ON MOVE");

            var moveVal = value.Get<Vector2>();

            desiredMovement = moveVal * speed;
        }

        private Vector3 lastFramePosition;

        private void FixedUpdate()
        {
            GroundedCheck();
            UpdateMovementVector();
            AlignForwardWithMovementDirection();
        }

        private void Update()
        {
            BodyBob();
        }

        private void LateUpdate()
        {
            MeasureDistance();
            PlaceFeetOnGroundAfterMovementDone();
            PlaceFeetOnGroundAfterJump();
        }


        private float distanceCovered = 0f;
        private float lastStepIntervalRight = 0f;
        private float lastStepIntervalLeft = 0;

        [SerializeField] private float stepSize = 0.3f;


        private bool groundedLastFrame = false;
        private Vector2 movementDirectionLastFrame;

        private PlayerInput playerInput;

        private void PlaceFeetOnGroundAfterMovementDone()
        {
            if (Grounded && movementDirectionLastFrame.magnitude > 0.001f && desiredMovement.magnitude < 0.001f)
            {
                UpdateFeetPosition(false);
                InitStepIntervals();
            }

            movementDirectionLastFrame = desiredMovement;
        }

        private void PlaceFeetOnGroundAfterJump()
        {
            if (groundedLastFrame == false && Grounded == true)
            {
                UpdateFeetPosition(true);
                InitStepIntervals();
            }

            groundedLastFrame = Grounded;
        }

        private void MeasureDistance()
        {
            if (!Grounded)
            {
                return;
            }

            var framePositionWithoutY = new Vector3(transform.position.x, 0f, transform.position.z);
            var distance = Time.deltaTime * (desiredMovement / speed).magnitude;

            distanceCovered += distance;
            lastStepIntervalLeft += distance;
            lastStepIntervalRight += distance;

            if (lastStepIntervalLeft > stepSize)
            {
                lastStepIntervalLeft -= stepSize;
                leftController.UpdateFootPosition();
            }

            if (lastStepIntervalRight > stepSize)
            {
                lastStepIntervalRight -= stepSize;
                rightController.UpdateFootPosition();
            }


            lastFramePosition = framePositionWithoutY;
        }

        private void BodyBob()
        {
            var movementMultiplier = Grounded ? (movementDirection / speed).magnitude : 0f;
            sinValue += Time.deltaTime * (bobSpeed + movementMultiplier * movementSpeedBobInfluenceFrequency)  * 60f;

            bodyTransform.localScale = new Vector3(bodyTransform.localScale.x,
                startPos.y + Mathf.Sin(sinValue) * (bobAmplitude +
                (movementMultiplier * movementSpeedBobInfluenceAmplitutde)),
                bodyTransform.localScale.z);
        }

        private void GroundedCheck()
        {
            var amount = Physics.OverlapSphereNonAlloc(groundCheck.position, 0.1f, groundCheckArray, groundLayerMask);
            Grounded = amount > 0;
        }

        private void UpdateMovementVector()
        {
            movementDirection = Vector2.Lerp(movementDirection, desiredMovement, movementChangeSmoothing);
            rigidbody.velocity = new Vector3(movementDirection.x, rigidbody.velocity.y, movementDirection.y);
        }

        private void AlignForwardWithMovementDirection()
        {
            transform.forward = Vector3.Lerp(transform.forward,
                new Vector3(rigidbody.velocity.x, 0f, rigidbody.velocity.z), forwardAlignmentSmoothing);
        }
    }
}