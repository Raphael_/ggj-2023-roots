using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace roots
{
    public class PlayerDeathManager : MonoBehaviour
    {
        [SerializeField] private Transform playerSpawnPoint;

        public readonly UnityEvent OnPlayerDied = new();

        private List<Action> actionsUponDeath = new();
        private List<Action> actionsUponRespawn = new();

        public int timesDied { get; private set; } = 0;


        private void Start()
        {
            TimeManager.Instance.OnTimerExpired.AddListener(Die);
        }

        public void DelayedDeath(float delayedBy)
        {
            Invoke(nameof(Die), delayedBy);
        }

        public void Die()
        {
            OnPlayerDied.Invoke();
            transform.position = playerSpawnPoint.position;
            transform.rotation = Quaternion.identity;
            gameObject.SetActive(false);
            
            Invoke(nameof(ExecuteOnDeaths), 0.15f);

            if (EndController.Instance.GameEnds)
            {
                return;
            }
            Invoke(nameof(Respawn), 0.20f);
        }

        public void DoOnNextDeath(Action action)
        {
            actionsUponDeath.Add(action);
        }

        private void ExecuteOnDeaths()
        {
            foreach (var action in actionsUponDeath)
            {
                action();
            }
                
            actionsUponDeath.Clear();
        }

        private void Respawn()
        {
            timesDied++;
            gameObject.SetActive(true);
            Player.Instance.playerSoundController.PlayDeathSound();
            Invoke(nameof(ExecuteOnRespawn), 0.15f);
            TimeManager.Instance.ResetAndStartTimer();
        }
        
        public void DoOnNextRespawn(Action action)
        {
            actionsUponRespawn.Add(action);
        }
        
        private void ExecuteOnRespawn()
        {
            Debug.Log("Execute on respawn!");
            foreach (var action in actionsUponRespawn)
            {
                action();
            }
                
            actionsUponRespawn.Clear();
        }
    }
}
