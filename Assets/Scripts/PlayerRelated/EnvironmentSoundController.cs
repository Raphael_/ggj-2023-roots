﻿using System.Collections.Generic;
using UnityEngine;

namespace roots.PlayerRelated
{
    public class EnvironmentSoundController : MonoBehaviour
    {
        private AudioSource audioSource;
        public static EnvironmentSoundController Instance;
        
        private void Awake()
        {
            Instance = this;
            audioSource = GetComponent<AudioSource>();
        }
        
        Queue<AudioClip> clipQueue = new();
 
        void Update()
        {
            if (audioSource.isPlaying == false && clipQueue.Count > 0) {
                audioSource.clip = clipQueue.Dequeue();
                audioSource.Play();
            }
        }
        public void PlaySound(AudioClip clip)
        {
            clipQueue.Enqueue(clip);
        }
    }
}