using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Serialization;

namespace roots
{
    public class PlayerSoundController : SerializedMonoBehaviour
    {
        private AudioSource AudioSource;

        [OdinSerialize] private Dictionary<string, AudioClip> GroundTagClips = new();

        [SerializeField] [Range(0.0f, 1.0f)] private float stepSoundVolume;
        [SerializeField] [Range(0.0f, 1.0f)] private float stepPitchVariation = 0.2f;

        [SerializeField] [Range(0.0f, 1.0f)] private float interactionSoundVolume;

        [SerializeField] private AudioClip pickUpClip;

        [SerializeField] private AudioClip actionClip;

        [FormerlySerializedAs("interactionSucceededClip")] [SerializeField]
        private AudioClip fulfilledClip;

        [SerializeField] private AudioClip interactionFailedClip;

        [SerializeField] private AudioClip deathClip;
        
        [SerializeField] private AudioClip jumpClip;

        [FormerlySerializedAs("layerMask")] [SerializeField]
        private LayerMask groundLayerMask;

        // Start is called before the first frame update
        void Start()
        {
            AudioSource = GetComponent<AudioSource>();
            foreach (var c in GetComponents<PlayerFeetController>())
            {
                c.OnFootStep.AddListener(PlayStepSound);
            }
        }

        // Update is called once per frame
        void Update()
        {
        }

        private void PlayStepSound()
        {
            if (Physics.Raycast(transform.position, Vector3.down * 3f, out var info, 20f, groundLayerMask))
            {
                var steppedOnGameObject = info.transform.gameObject;
                foreach (var (t, c) in GroundTagClips)
                {
                    if (steppedOnGameObject.CompareTag(t))
                    {
                        AudioSource.pitch = Random.Range(1f - stepPitchVariation * .5f, 1f + stepPitchVariation * .5f);
                        AudioSource.PlayOneShot(c, stepSoundVolume * Random.Range(0.8f,1.1f));
                        break;
                    }
                }
            }
        }

        public void PlayPickUpSound()
        {
            AudioSource.pitch = 1f;
            AudioSource.clip = pickUpClip;
            AudioSource.volume = interactionSoundVolume;
            AudioSource.Play();
        }

        public void PlayInteractionFailedSound()
        {
            AudioSource.pitch = 1f;
            AudioSource.PlayOneShot(interactionFailedClip, interactionSoundVolume);
        }

        public void PlayFulfilledSound(AudioClip overrideFulfilledClip = null)
        {
            AudioSource.pitch = 1f;
            AudioSource.PlayOneShot(overrideFulfilledClip != null ? overrideFulfilledClip : fulfilledClip,  interactionSoundVolume);
        }

        public void PlayDeathSound()
        {
            AudioSource.pitch = 1f;
            AudioSource.PlayOneShot(deathClip, interactionSoundVolume);

        }
        
        public void PlayJumpSound()
        {
            AudioSource.pitch = 1f;
            AudioSource.PlayOneShot(jumpClip, stepSoundVolume);
        }
    }
}