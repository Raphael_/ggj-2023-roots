using System;
using DG.Tweening;
using roots.Utils;
using UnityEngine;

namespace roots
{
    public class InteractionManager : MonoBehaviour
    {
        [SerializeField] private Transform interactionCheckOrigin;
        [SerializeField] private float interactionCheckRadius = 1.3f;

        private Collider[] checkArray = new Collider[15];

        [SerializeField] private Canvas interactionPrompt;
    

        private void OnInteract()
        {
            if (LookForNearestInteractable(out var interactable))
            {
                if (interactable.Interact() == false)
                {
                    NegativeFeedbackOnInteract();
                }
            }
        }

        private void NegativeFeedbackOnInteract()
        {
            interactionPrompt.transform.DOShakePosition(0.7F, Vector3.right*0.3F, 10, 0F);
            Player.Instance.playerSoundController.PlayInteractionFailedSound();
        }

        private bool LookForNearestInteractable(out Interactable nearest)
        {
            Physics.queriesHitTriggers = true;
            var numFound = Physics.OverlapSphereNonAlloc(interactionCheckOrigin.position, interactionCheckRadius, checkArray);
            if (numFound > 0)
            {
                //print(numFound);
                Interactable found = null;
                float currenDistance = float.MaxValue;
                foreach (var coll in checkArray)
                {
                    if (coll == null)
                    {
                        continue;
                    }
                    //print(coll.gameObject.name);
                    if (coll.gameObject.HasComponent<Interactable>(out var interactable))
                    {
                        if (interactable.enabled == false)
                        {
                            continue;
                        }
                        var dist = Vector3.Distance(transform.position, coll.transform.position);
                        if (dist < currenDistance)
                        {
                            found = interactable;
                            currenDistance = dist;
                        }
                    }
                }

                if (found != null)
                {
                    Physics.queriesHitTriggers = false;
                    nearest = found;
                    return true;
                }
            }
            Physics.queriesHitTriggers = false;
            nearest = null;
            return false;
        }

        private void FixedUpdate()
        {
            if (LookForNearestInteractable(out var interactable))
            {
                interactionPrompt.gameObject.SetActive(true);
            }
            else
            {
                interactionPrompt.gameObject.SetActive(false);
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1f, 0f, 0f, 0.55f);
            Gizmos.DrawSphere(interactionCheckOrigin.position, interactionCheckRadius);
        }
    }
}