using UnityEngine;

namespace roots
{
        [RequireComponent(typeof(LineRenderer))]
        public class LineRendererConnector : MonoBehaviour
        {
                private new LineRenderer renderer;
        
                [SerializeField] private Transform connectorStart;
                [SerializeField] private Transform connectorEnd;

                private void Awake()
                {
                        renderer = GetComponent<LineRenderer>();
                }

                private void Update()
                {
                        renderer.SetPositions(new []{connectorStart.position, connectorEnd.position});
                }
        }
}