using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class TimeManager : MonoBehaviour
{
    public const int RoundTimeInSeconds = 60;
    private float _Timer = 0;
    public float totalTime { get; private set; } = 0f;

    public float Timer => _Timer;

    private bool TimerStarted = false;

    public static TimeManager Instance;

    public readonly UnityEvent OnTimerExpired = new UnityEvent();
    public readonly UnityEvent OnReachedBelowTen = new UnityEvent();

    private bool killSelfTriggered = false;

    private bool reachedBelowTen = false;
    
    private bool paused = false;
    public void PauseTimer()
    {
        paused = true;
    }

    public void UnpauseTimer()
    {
        paused = false;
    }

    private void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetAndStartTimer();
    }

    [Button]
    private void SubstractTenSeconds()
    {
        _Timer -= 10f;
    }
    
    [Button]
    private void AddTenSeconds()
    {
        _Timer += 10f;
    }

    private void OnRespawn()
    {
        killSelfTriggered = true;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (!TimerStarted) return;


        if (!paused)
        {
            _Timer -= Time.deltaTime;
            if (killSelfTriggered)
            {
                killSelfTriggered = false;
                _Timer = -1f;
            }
        }

        totalTime += Time.timeScale * Time.deltaTime;

        if (reachedBelowTen == false && _Timer < 10.6f && _Timer > 0f)
        {
            reachedBelowTen = true;
            OnReachedBelowTen.Invoke();
        }
        
        if (_Timer <= 0)
        {
            TimerStarted = false;
            _Timer = 0;
            OnTimerExpired.Invoke();
        }
    }

    public void ResetAndStartTimer()
    {
        reachedBelowTen = false;
        _Timer = RoundTimeInSeconds;
        TimerStarted = true;
    }
}
