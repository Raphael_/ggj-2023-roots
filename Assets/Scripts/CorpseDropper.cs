using UnityEngine;

namespace roots
{
    [RequireComponent(typeof(PlayerDeathManager))]
    public class CorpseDropper : MonoBehaviour
    {
        private PlayerDeathManager playerDeathManager;

        [SerializeField] private GameObject corpse;
        [SerializeField] private LayerMask layerMask;

        private void Awake()
        {
            playerDeathManager = GetComponent<PlayerDeathManager>();
        }

        private void Start()
        {
            playerDeathManager.OnPlayerDied.AddListener(() =>
            {
                if (Physics.Raycast(transform.position, Vector3.down * 3f, out var info, 20f, layerMask))
                {
                    Instantiate(corpse, info.point, transform.rotation);
                }
            });
        }
    }
}