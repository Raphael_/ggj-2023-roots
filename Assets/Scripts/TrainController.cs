using System;
using UnityEngine;

namespace roots
{
    [RequireComponent(typeof(Animator))]
    public class TrainController : MonoBehaviour
    {
        private Animator animator;

        [SerializeField] private float timeUntilTrain = 48f;

        [SerializeField] private AudioSource trainWhistle;

        private float timeForTrain;

        private bool trainWentAlready = false;
        private static readonly int ChooChoo = Animator.StringToHash("ChooChoo");

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }


        private void Start()
        {
            SetTimeForTrain();
            
            Player.Instance.playerDeathManager.OnPlayerDied.AddListener(() =>
            {
                SetTimeForTrain();
            });
        }

        private void SetTimeForTrain()
        {
            trainWentAlready = false;
            timeForTrain = Time.time + timeUntilTrain;
        }

        private void Update()
        {
            if (EndController.Instance.GameEnds) return;
            if (trainWentAlready == false && Time.time > timeForTrain)
            {
                trainWentAlready = true;
                animator.SetTrigger(ChooChoo);
                trainWhistle.Play();
            }
        }
    }
}