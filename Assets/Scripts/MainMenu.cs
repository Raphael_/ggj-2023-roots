using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace roots
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] Slider volumeSlider;
        [SerializeField] private Image loadingCircleImage;

        [SerializeField] private Transform ui;
        [SerializeField] private Transform loadingUI;

        void Start()
        {
            Time.timeScale = 1f;
            
            if(!PlayerPrefs.HasKey("Volume")){
                PlayerPrefs.SetFloat("Volume",1);
            }
        }

        public void ChangeVolume(){
            AudioListener.volume = volumeSlider.value;
            Save();
        }

        private void Load(){
            volumeSlider.value = PlayerPrefs.GetFloat("Volume");
        }
        
        private void Save(){
            PlayerPrefs.SetFloat("Volume", volumeSlider.value);
        }

        public void PlayGame()
        {
            AsyncOperation loadSceneAsync = SceneManager.LoadSceneAsync(1);
            ui.gameObject.SetActive(false);
            loadingUI.gameObject.SetActive(true);
            StartCoroutine(LoadingBar(loadSceneAsync));
        }

        IEnumerator LoadingBar(AsyncOperation asyncOperation)
        {
            while (asyncOperation.progress < 0.99f)
            {
                loadingCircleImage.fillAmount = asyncOperation.progress;
                yield return new WaitForEndOfFrame();
            }
        }
        
        public void QuitGame()
        {
            Application.Quit();
        }
        public void LoadMainMenu(){
            SceneManager.LoadScene(0);
        }
    }
}
