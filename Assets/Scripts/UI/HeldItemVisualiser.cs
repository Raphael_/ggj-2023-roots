using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace roots.UI
{
    public class HeldItemVisualiser : MonoBehaviour
    {
        [SerializeField] private TMP_Text text;
        [SerializeField] private Image image;

        private void Start()
        {
            Player.Instance.itemInventory.onHeldItemChanged.AddListener(item =>
            {
                if (item == null)
                {
                    image.enabled = false;
                    text.enabled = false;
                    return;
                }
                
                if (item.sprite == null)
                {
                    image.enabled = false;
                    text.enabled = true;

                    text.text = item.itemName;
                }
                else
                {
                    image.enabled = true;
                    text.enabled = false;

                    image.sprite = item.sprite;
                }
            });
        }
    }
}