using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace roots.UI
{
    [RequireComponent(typeof(RectTransform))]
    public class TutorialManager : MonoBehaviour
    {
        private RectTransform tutorialBox;
        [SerializeField] private TMP_Text text;

        public static TutorialManager Instance;

        private Queue<string> backlog = new();

        private bool busy = false;

        private void Awake()
        {
            tutorialBox = GetComponent<RectTransform>();
            Instance = this;
        }

        private void Start()
        {
            SetupSuicideTutorial();
        }

        private void SetupSuicideTutorial()
        {
            // Player.Instance.playerDeathManager.DoOnNextDeath(() =>
            // {
            //     Invoke(nameof(ShowSuicideTutorial), 3f);
            // });
        }

        private void ShowSuicideTutorial()
        {
            DisplayMessage("Press 'K' to commit unalive");

        }

        public void DisplayMessage(string message)
        {
            backlog.Enqueue(message);
        }

        private void ActuallyShowMessage(string message)
        {
            busy = true;
            text.text = message;
            tutorialBox.DOAnchorPosY(-56f, 0.5f).SetEase(Ease.OutElastic, 0.02f, 0.3f).OnComplete(() =>
            {
                tutorialBox.DOAnchorPosY(56f, 0.3f).SetDelay(4.8f).OnComplete(() =>
                {
                    busy = false;
                });
            });
        }
        
        

        private void Update()
        {
            if (backlog.Count > 0 && !busy)
            {
                ActuallyShowMessage(backlog.Dequeue());
            }
        }
    }
}