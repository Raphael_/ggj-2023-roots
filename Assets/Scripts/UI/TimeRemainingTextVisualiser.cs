using TMPro;
using UnityEngine;

namespace roots.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class TimeRemainingTextVisualiser : MonoBehaviour
    {
        private TMP_Text text;
        private void Awake()
        {
            text = GetComponent<TMP_Text>();
        }

        private void Update()
        {
            text.text = $"{TimeManager.Instance.Timer:n0}";
        }
    }
}
