using System;
using UnityEngine;
using UnityEngine.UI;

namespace roots.UI
{
    public class TransitionWipe : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        private static readonly int WipeRight = Animator.StringToHash("WipeRight");
        private static readonly int WipeLeft = Animator.StringToHash("WipeLeft");

        private void Start()
        {
            Teleporter.OnTeleported += (sender, wipeDir) =>
            {
                if (wipeDir)
                {
                    animator.SetTrigger(WipeRight);
                }
                else
                {
                    animator.SetTrigger(WipeLeft);
                }
            };
        }
    }
}