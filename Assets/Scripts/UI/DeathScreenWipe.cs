using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace roots.UI
{
    public class DeathScreenWipe : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TMP_Text text;
        [SerializeField] private TMP_Text totalTime;
        [SerializeField] private TMP_Text timesDied;
        
        private void Start()
        {
            Player.Instance.playerDeathManager.OnPlayerDied.AddListener(() =>
            {
                image.fillClockwise = true;
                if (EndController.Instance.GameEnds)
                {
                    text.text = "The end";

                    totalTime.DOFade(1f, .7f).SetDelay(1f);
                    timesDied.DOFade(1f, .6f).SetDelay(1.2f);

                    var t = TimeSpan.FromSeconds(TimeManager.Instance.totalTime);
                    totalTime.text = $"Time: {t.TotalMinutes:N0}min {t.Seconds}s {t.Milliseconds}ms";
                    timesDied.text = $"Times respawned: {Player.Instance.playerDeathManager.timesDied}";
                }
                image.DOFillAmount(1f, 0.32f).OnComplete(() =>
                {
                    if (EndController.Instance.GameEnds)
                    {
                        return;
                    }
                    image.fillClockwise = false;
                    image.DOFillAmount(0f, 0.32f).SetDelay(0.28f);
                });
            });
        }
    }
}