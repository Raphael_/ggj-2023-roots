using UnityEngine;
using UnityEngine.UI;

namespace roots.UI
{
    public class TimeBar : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] private Image sliderImage;

        // Update is called once per frame
        void Update()
        {
            sliderImage.fillAmount = TimeManager.Instance.Timer / TimeManager.RoundTimeInSeconds;
        }
    }
}
