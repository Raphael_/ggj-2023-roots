using System;
using System.Collections.Generic;
using roots.UI;
using roots.Utils;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.Events;

namespace roots
{
    public class ItemInventory : MonoBehaviour
    {
        [field:SerializeField] public Item heldItem { get; private set; }
        public UnityEvent<Item> onHeldItemChanged { get; private set; } = new();

        [SerializeField] private Transform heldSpot;

        private List<Item> pickedUpSoFar = new();

        private void Start()
        {
            Player.Instance.playerDeathManager.OnPlayerDied.AddListener(() =>
            {
                if (heldItem != null)
                {
                    heldItem.OnPlayerDeath();
                }
            });
        }

        private void OnDrop()
        {
            if (heldItem != null)
            {
                DropItem();
            }
        }

        public bool TryPickUpItem(Item newItem)
        {
            if (heldItem != null)
            {
                return false;
            }
            
            ChangeItem(newItem);
            
            if (newItem != null)
            {
                var instantiate = Instantiate(newItem.itemPickupPrefab, heldSpot);
                instantiate.transform.localPosition = Vector3.zero;

                if (instantiate.HasComponent<Interactable>(out var interactable))
                {
                    interactable.enabled = false;
                }
            }

            return true;
        }

        private void ChangeItem(Item item)
        {
            heldItem = item;

            if (item != null)
            {
                if (pickedUpSoFar.Contains(item) == false)
                {
                    pickedUpSoFar.Add(item);
                    if (item.firstTimePickupTutorial.IsNullOrWhitespace() == false)
                    {
                        TutorialManager.Instance.DisplayMessage(item.firstTimePickupTutorial);
                    }
                }
            }

            onHeldItemChanged.Invoke(heldItem);
        }


        private void DropItem()
        {
            if (heldItem != null)
            {
                Instantiate(heldItem.itemPickupPrefab, transform.position, transform.rotation);
                DestroyHeldItem();
            }
        }

        public void DestroyHeldItem()
        {
            if (heldSpot.childCount > 0)
            {
                Destroy(heldSpot.GetChild(0).gameObject);
            }
            
            ChangeItem(null);
        }
    }
}