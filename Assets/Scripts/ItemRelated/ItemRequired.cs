using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;

namespace roots.ItemRelated
{
    [RequireComponent(typeof(Interactable))]
    public class ItemRequired : SerializedMonoBehaviour, IInteractableSubComponent
    {
        private Interactable interactable;

        [SerializeField] private Item requiredItem;

        [SerializeField] private bool destroyItemOnUse = false;
        [SerializeField] private bool disableInteractableOnFulfilled = true;
        [SerializeField] private bool onlyOnce = true;
        [SerializeField] private bool resetOnDeath = false;
        [SerializeField, HideIf(nameof(onlyOnce))] private int requiredTimesFulfilled = 1;
        [SerializeField] [CanBeNull] private AudioClip overrideFulfilledClip = null;

        private int timesFulfilled = 0;

        public bool fulfilled => onlyOnce ? timesFulfilled >= 1 : timesFulfilled >= requiredTimesFulfilled;

        public readonly UnityEvent RequiredItemUsed = new();
        public readonly UnityEvent OnRequirementFulfilled = new();
        public readonly UnityEvent OnInteractedWithoutRequirement = new UnityEvent();

        [OdinSerialize] 
        private List<InteractConsequences> consequences = new();

        [SerializeField] private ItemPickup itemPickup;
        

        private void Awake()
        {
            itemPickup = GetComponent<ItemPickup>();
            interactable = GetComponent<Interactable>();
        }

        private void Start()
        {
            if (resetOnDeath)
            {
                Player.Instance.playerDeathManager.OnPlayerDied.AddListener(() =>
                {
                    timesFulfilled = 0;
                    interactable.enabled = true;
                });
            }
        }

        public bool OnInteract()
        {
            if (timesFulfilled>0 && onlyOnce)
            {
                return false;
            }

            bool heldItemAtBeginning = Player.Instance.itemInventory.heldItem != null;

            if (Player.Instance.itemInventory.heldItem == requiredItem || requiredItem == null) 
            {
                if (destroyItemOnUse && requiredItem != null)
                {
                    Player.Instance.itemInventory.DestroyHeldItem();   
                }

                timesFulfilled++;
                RequiredItemUsed.Invoke();

                if (fulfilled)
                {
                    OnRequirementFulfilled.Invoke();
                    foreach (var consequence in consequences)
                    {
                        consequence.Effect(gameObject);
                    }
                }

                if (fulfilled && disableInteractableOnFulfilled)
                {
                    interactable.enabled = false;
                    Player.Instance.playerSoundController.PlayFulfilledSound(overrideFulfilledClip);
                }

                return true;
            }
            else
            {
                OnInteractedWithoutRequirement.Invoke();
            }
            
            if (heldItemAtBeginning == false && itemPickup != null)
            {
                return true;
            }
            
            return false;
        }
    }
}