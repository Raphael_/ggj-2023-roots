using UnityEngine;

namespace roots.ItemRelated
{
    public class DestroyGameObjectConsequence : InteractConsequences
    {
        public override void Effect(GameObject gameObject)
        {
            GameObject.Destroy(gameObject);
        }
    }
}