using System.Collections.Generic;
using roots.PlayerRelated;
using UnityEngine;

namespace roots.ItemRelated
{
    public class PlaySoundConsequence : InteractConsequences
    {
        [SerializeField]
        private List<AudioClip> clips;
        public override void Effect(GameObject gameObject)
        {
            foreach (var clip in clips)
            {
                EnvironmentSoundController.Instance.PlaySound(clip);
            }
        }
    }
}