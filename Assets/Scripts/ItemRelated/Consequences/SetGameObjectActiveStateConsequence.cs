using UnityEngine;

namespace roots.ItemRelated
{
    public class SetGameObjectActiveStateConsequence : InteractConsequences
    {
        public Transform target;
        public bool desiredState;

        public override void Effect(GameObject gameObject)
        {
            target.gameObject.SetActive(desiredState);
        }
    }
}