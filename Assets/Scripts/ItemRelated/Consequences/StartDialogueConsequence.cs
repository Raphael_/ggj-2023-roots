using roots.Dialogue;
using UnityEngine;

namespace roots.ItemRelated
{
    public class StartDialogueConsequence : InteractConsequences
    {
        public DialogueTrigger dialogueTrigger;

        public override void Effect(GameObject gameObject)
        {
            dialogueTrigger.StartDialog();
        }
    }
}