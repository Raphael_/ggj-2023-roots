using System;
using UnityEngine;

namespace roots.ItemRelated
{
    [Serializable]
    public abstract class InteractConsequences
    {
        public abstract void Effect(GameObject gameObject);
    }
}