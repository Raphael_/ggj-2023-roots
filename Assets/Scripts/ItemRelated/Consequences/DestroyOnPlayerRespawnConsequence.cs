using UnityEngine;

namespace roots.ItemRelated
{
    public class DestroyOnPlayerRespawnConsequence : InteractConsequences
    {
        // Watch out for execution order, call this before player death consequence
        public override void Effect(GameObject gameObject)
        {
            Player.Instance.playerDeathManager.DoOnNextRespawn(() =>
            {
                Debug.Log("Destroyed!");
                GameObject.Destroy(gameObject);
            });
        }
    }
}