﻿using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class SetActiveOnNextRespawnConsequence : InteractConsequences
    {
        public Transform target;
        public bool desiredState;

        public override void Effect(GameObject gameObject)
        {
            Player.Instance.playerDeathManager.DoOnNextRespawn(() => { target.gameObject.SetActive(desiredState); });
        }
    }
}