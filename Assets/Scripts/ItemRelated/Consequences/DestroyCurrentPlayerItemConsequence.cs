using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class DestroyCurrentPlayerItemConsequence : InteractConsequences
    {
        public override void Effect(GameObject gameObject)
        {
            Player.Instance.itemInventory.DestroyHeldItem();
        }
    }
}