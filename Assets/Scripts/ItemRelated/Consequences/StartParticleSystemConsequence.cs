using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class StartParticleSystemConsequence : InteractConsequences
    {
        public ParticleSystem particleSystem;
        
        public override void Effect(GameObject gameObject)
        {
            particleSystem.Play();
        }
    }
}