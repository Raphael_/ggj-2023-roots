using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class StopParticleSystemConsequence : InteractConsequences
    {
        public ParticleSystem particleSystem;
        
        public override void Effect(GameObject gameObject)
        {
            particleSystem.Stop();
        }
    }
}