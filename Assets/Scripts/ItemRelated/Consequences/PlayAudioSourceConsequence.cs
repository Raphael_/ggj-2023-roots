using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class PlayAudioSourceConsequence : InteractConsequences
    {

        public AudioSource AudioSource;
        
        public override void Effect(GameObject gameObject)
        {
            AudioSource.Play();
        }
    }
}