using System.Dynamic;
using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class PlayerDeathConsequence : InteractConsequences
    {
        public float delayedBy = 0;
        public override void Effect(GameObject gameObject)
        {
            Player.Instance.playerDeathManager.DelayedDeath(delayedBy);
        }
    }
}