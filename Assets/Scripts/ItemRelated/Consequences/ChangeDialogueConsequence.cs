using roots.Dialogue;
using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class ChangeDialogueConsequence : InteractConsequences
    {
        public DialogueData newData;
        public DialogueTrigger DialogueTrigger;

        public bool effectAfterDeath = false;



        public override void Effect(GameObject gameObject)
        {
            if (effectAfterDeath)
            {
                Player.Instance.playerDeathManager.DoOnNextDeath(() =>
                {
                    DialogueTrigger.data = newData;
                });
            }
            else
            {
                DialogueTrigger.data = newData;
            }
            
        }
    }
}