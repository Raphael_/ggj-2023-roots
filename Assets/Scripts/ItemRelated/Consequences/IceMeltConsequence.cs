using DG.Tweening;
using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class IceMeltConsequence : InteractConsequences
    {
        
        public override void Effect(GameObject gameObject)
        {
            gameObject.transform.DOScale(0.05f, 8f).OnComplete(() =>
            {
                gameObject.SetActive(false);
            });
        }
    }
}