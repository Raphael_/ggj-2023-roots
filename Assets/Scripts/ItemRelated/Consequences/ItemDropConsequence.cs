using UnityEngine;

namespace roots.ItemRelated
{
    public class ItemDropConsequence : InteractConsequences
    {
        public Item toBeDropped;

        public Vector3 offset;
        
        public override void Effect(GameObject gameObject)
        {
            GameObject.Instantiate(toBeDropped.itemPickupPrefab, gameObject.transform.position + offset, Quaternion.identity);
        }
    }
}