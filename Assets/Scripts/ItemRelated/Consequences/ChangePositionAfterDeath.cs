using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class ChangePositionAfterDeath : InteractConsequences
    {
        public Transform Target;
        public Transform NewPosition;
        
        public override void Effect(GameObject gameObject)
        {
            Player.Instance.playerDeathManager.DoOnNextDeath(() =>
            {
                Target.position = NewPosition.position;
            });
        }
    }
}