using UnityEngine;

namespace roots.ItemRelated.Consequences
{
    public class SetAnimatorBoolConsequence : InteractConsequences
    {
        public Animator animator;
        public string valueName;
        public bool endValue;
        
        public override void Effect(GameObject gameObject)
        {
            animator.SetBool(valueName, endValue);
        }
    }
}