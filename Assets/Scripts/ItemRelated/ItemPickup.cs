using System;
using System.Collections.Generic;
using roots.ItemRelated;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace roots
{
    [RequireComponent(typeof(Interactable))]
    public class ItemPickup : SerializedMonoBehaviour, IInteractableSubComponent
    {
        [SerializeField] private Item item;
        private AudioSource audioSource;

        private Interactable interactable;

        [OdinSerialize] private List<InteractConsequences> consequences = new();

        [SerializeField] private bool onInteract = true;

        private ItemRequired itemRequired;

        private void Awake()
        {
            interactable = GetComponent<Interactable>();
            itemRequired = GetComponent<ItemRequired>();
        }

        public bool OnInteract()
        {
            if (!onInteract) return true;

            if (itemRequired == null)
            {
                if (Player.Instance.itemInventory.TryPickUpItem(item))
                {
                    OnItemSuccessfullyPickedUp();
                    return true;
                }
            }

            if (itemRequired != null)
            {
                return true;
            }
            return false;
        }
        
        private void Start()
        {
            if (itemRequired != null)
            {
                itemRequired.OnInteractedWithoutRequirement.AddListener(() =>
                {
                    if (!onInteract) return;

                    if (Player.Instance.itemInventory.TryPickUpItem(item))
                    {
                        OnItemSuccessfullyPickedUp();
                    }
                });
            }
        }

        private void OnItemSuccessfullyPickedUp()
        {
            Player.Instance.playerSoundController.PlayPickUpSound();
            foreach (var consequence in consequences)
            {
                Debug.Log("Consequence");
                consequence.Effect(gameObject);
            }

            gameObject.SetActive(false);
        }

        
    }
}