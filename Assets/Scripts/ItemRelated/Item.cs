using System.Collections.Generic;
using roots.ItemRelated;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace roots
{
    [CreateAssetMenu]
    public class Item : SerializedScriptableObject
    {
       [field: SerializeField] 
        public string itemName { get; private set; }
        [field: SerializeField] 
        public Sprite sprite { get; private set; }
        [field: SerializeField] 
        public GameObject itemPickupPrefab { get; private set; }
        
        [field: SerializeField] 
        public string firstTimePickupTutorial { get; private set; }

        [OdinSerialize] private List<InteractConsequences> onPlayerDeathConsequences = new();

        public void OnPlayerDeath()
        {
            foreach (var consequence in onPlayerDeathConsequences)
            {
                consequence.Effect(Player.Instance.gameObject);
            }
        }
    }
}