using System;
using DG.Tweening;
using UnityEngine;

namespace roots
{
    [RequireComponent(typeof(TimeManager))]
    public class TimeRunningOutEffect : MonoBehaviour
    {
        private TimeManager timeManager;

        [SerializeField] private AudioSource belowTenSound;
        [SerializeField] private AudioSource expiredSound;
        [SerializeField] private AudioSource music;

        private float startVolume;

        private void Awake()
        {
            timeManager = GetComponent<TimeManager>();
            startVolume = music.volume;
        }

        private void Start()
        {
            timeManager.OnReachedBelowTen.AddListener(() =>
            {
                belowTenSound.Play();
                music.DOFade(0.12f, 2f);
            });
            
            timeManager.OnTimerExpired.AddListener(() =>
            {
                music.DOFade(startVolume, 1.5f);
                belowTenSound.Stop();
                expiredSound.Play();
            });
        }
    }
}