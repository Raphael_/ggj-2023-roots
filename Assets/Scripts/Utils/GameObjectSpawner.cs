using UnityEngine;

namespace roots.Utils
{
    public class GameObjectSpawner : MonoBehaviour
    {
        private GameObject toBeSpawned;

        public void Spawn()
        {
            Instantiate(toBeSpawned, transform.position, Quaternion.identity);
        }
    }
}