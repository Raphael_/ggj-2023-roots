using UnityEngine;

namespace roots.Utils
{
    public class DestroyGameObject : MonoBehaviour
    {
        public void DoDestroy()
        {
            Destroy(gameObject);
        }
    }
}