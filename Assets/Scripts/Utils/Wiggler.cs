using System;
using UnityEngine;

namespace roots.Utils
{
    public class Wiggler : MonoBehaviour
    {
        
        
        private Vector3 startPos;

        private float sinValue;
        
        [SerializeField] private float bobSpeed = 0.16f;
        [SerializeField] private float bobAmplitude = 0.02f;



        private void Awake()
        {
            startPos = transform.localScale;
        }

        private void Update()
        {
            BodyBob();
        }


        private void BodyBob()
        {
            sinValue += Time.deltaTime * bobSpeed * 60f;

            transform.localScale = new Vector3(transform.localScale.x,
                startPos.y + Mathf.Sin(sinValue)*bobAmplitude,
                transform.localScale.z);
        }
        
    }
}