using DG.Tweening;
using Micosmo.SensorToolkit;
using roots.PlayerRelated;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.Rendering;

namespace roots
{
    public class EndController : MonoBehaviour
    {
        
        public AudioSource endAmbient;
        
        public static EndController Instance;

        public bool GameEnds = false;
        [SerializeField] private Collider blocker;
        
        private void Awake()
        {
            Instance = this;
        }

        public void AreaEntered()
        {
            endAmbient.Play();
            GetComponent<RangeSensor>().enabled = false;
            blocker.gameObject.SetActive(true);
            TimeManager.Instance.PauseTimer();

            GameEnds = true;
        }
    }
}
