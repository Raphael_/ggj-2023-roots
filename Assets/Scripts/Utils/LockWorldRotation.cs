using System;
using UnityEngine;

namespace roots.Utils
{
    public class LockWorldRotation : MonoBehaviour
    {
        [SerializeField] private Quaternion lockedRotation;


        [ContextMenu("Set locked Rotation")]
        private void LockRotation()
        {
            lockedRotation = transform.rotation;
        }
        
        private void Update()
        {
            transform.rotation = lockedRotation;
        }
    }
}