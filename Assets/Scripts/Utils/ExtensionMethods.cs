using UnityEngine;

namespace roots.Utils
{
    public static class ExtensionMethods {

        public static void ReplaceAllButY(this ref Vector3 value, Vector2 vector2)
        {
            value = new Vector3(vector2.x, value.y, vector2.y);
        }
     
        public static float Remap (this float value, float from1, float to1, float from2, float to2) {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }

        public static bool HasComponent<T>(this GameObject gameObject, out T component)
        {
            component = gameObject.GetComponent<T>();
            return component != null;
        }
        
        public static float InputToGetDesiredValue( this AnimationCurve animationCurve, float desiredValue, float precision = 0.001f, float startValue = 0f, float endValue = 1f)
        {
            for (float t = startValue; t < endValue; t += precision)
            {
                var value = animationCurve.Evaluate(t);
                if (value > desiredValue)
                {
                    return t;
                }
            }

            return endValue;
        }

        public static Color ReplaceAlpha(this Color color, float newAlpha)
        {
            return new Color(color.r, color.g, color.b, newAlpha);
        }
       
    }
}