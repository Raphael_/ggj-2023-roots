using System;
using UnityEngine;

namespace roots
{
    public class Teleporter : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private bool rightWipe = true;

        public static EventHandler<bool> OnTeleported;

        public void Teleport()
        {
            Player.Instance.transform.position = target.transform.position;
            OnTeleported?.Invoke(this, rightWipe);
        }
    }
}